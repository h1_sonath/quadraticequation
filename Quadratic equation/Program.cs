﻿﻿using System;

namespace Quadratic_equation
{
    class Program
    {
        static void Main(string[] args)
        {
            Program p = new Program();
            Console.WriteLine("Nhap so a: ");
            int a = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Nhap so b: ");
            int b = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Nhap so c: ");
            int c = Convert.ToInt32(Console.ReadLine());
            p.QuadraticEquation(a, b, c );
        }
        void QuadraticEquation(int a, int b, int c)
        {
            if (a == 0)
            {
                Console.WriteLine("Khong phai phuong trinh bac 2");
            }
            int delta = b*b - 4 * a * c;
            if (delta == 0)
            {
                double x = -(b / (2 * a));
                Console.WriteLine("Phuong trinh co nghiem kep la: x1 = x2 = " + x);
            }
            if(delta < 0)
            {
                Console.WriteLine("Phuong trinh khong co nghiem la so thuc");
            }
            if (delta > 0)
            {
                Console.WriteLine(delta);
                double x1 = (-b + Math.Sqrt(delta)) / (2 * a);
                double x2 = (-b - Math.Sqrt(delta)) / (2 * a);
                Console.WriteLine("Phuong trinh co 2 nghiem la: " + "x1 =" + x1 + " va x2 = " + x2);
            }
        }
    }
}
